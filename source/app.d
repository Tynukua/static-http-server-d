import std.stdio;
import mysocket:MySocket;
import std.concurrency:spawn;
import std.array:split;
import std.conv:to;

const string PAGES_ROOT = "./public";
const char[] PAGE_404 = import("./resource/404.html");
const char[] PAGE_403 = import("./resource/403.html");
const char[] PAGE_DIR = import("./resource/dir.html");


void main()
{
    auto server = MySocket.createServer();
    scope(exit) server.close();
    while(1){
        spawn(&run,server.accept.s);
    }

    

}

void run (int sock){
    import std.array:split;
    import std.algorithm:count;
    auto client = new MySocket(sock);
    scope(exit) client.close();
    char[1024] buf;
    string path = "";
    auto len = client.recv(buf);
    foreach(long i; 0..len){
        if(buf[i]!='\r' && buf[i]!= '\n')
            path~=buf[i];
        else break;
    }
    string[string] headers;
    headers["__TOP__"] = "HTTP/1.1 200 OK";
    headers["Content-Type"] = "text/html; charset=UTF-8";
    auto _body = getFile(path.split[1],headers);
    auto resp = getHeaders(headers) ~ '\n' ~ _body;
    client.send(resp);
    client.close;
}

char[] getFile(string path,ref string[string]headers){

    import std.file:isFile,isDir,exists;
    import std.path:chainPath,asRelativePath;
    import std.algorithm:canFind;
    import std.array:array;
    path = PAGES_ROOT~path;
    path.writeln;
    if(!path.exists){
        headers["__TOP__"] = "HTTP/1.1 404 Not Found";
        return []~PAGE_404;
    }
    if(path.canFind("..")){
        headers["__TOP__"] = "HTTP/1.1 403 Forbidden";
        return []~PAGE_403;
    }
    if(path.isFile){
        auto file = File(path,"r");
        bool end;
        char[] content = [];
        do{
            char[1024*1024]buf; 
            auto slice = file.rawRead(buf);
            content~=slice;
            end = slice.length<buf.length;

        }while (!end);
        file.close();
        return content;
    }
    if(path.isDir){
        return getDir(path);
    }
    headers["__TOP__"] = "HTTP/1.1 404 Not Found";
    return []~PAGE_404; 
}

char[] getDir(string path){
    import std.file:dirEntries,SpanMode,DirEntry;
    import std.format:format;
    import std.path:pathSplitter,buildPath;
    import std.array:array;
    string dirs = "";
    foreach (DirEntry f; dirEntries(path,SpanMode.shallow)){
        auto tmppath = f.name.pathSplitter.array[2..$].buildPath;
        
        dirs~=format!"<p><a href=/%s>\t/%s</a></p>"(tmppath,tmppath);

    }
    return dirs.format!(PAGE_DIR).to!(char[]);
}

char[] getHeaders(string[string] headers){
    import std.format:format;
    char[] result = [];
    result~=headers["__TOP__"]~"\r\n";
    headers.remove("__TOP__");
    foreach(k,v;headers){
        result ~= format!"%s : %s \r\n"(k,v);
    }
    return result;
}
