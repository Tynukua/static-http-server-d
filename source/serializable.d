interface Serializable(T){
    void recv(T sock);
    void send(T sock);
}
