static import serializable;
import std.stdio;
import std.conv:to;

import sock = std.socket;
import core.sys.posix.unistd:close;
alias Serializable = serializable.Serializable!MySocket;


class MySocket {
    
    int s;
    int opt;
    sock.sockaddr_in addr;
    static MySocket createServer(){
        auto s = new MySocket;
        s.s = sock.socket(sock.AF_INET,sock.SOCK_STREAM,0);

        //SET SOCK OPT
        if(sock.setsockopt(
            s.s, 
            sock.SOL_SOCKET, 
            (sock.SO_REUSEADDR| sock.SO_REUSEPORT),
            &s.opt, s.opt.sizeof)){
            throw new Exception("setsockopt(...) failed");
        }
        // BIND
        s.addr.sin_family = sock.AF_INET;
        s.addr.sin_addr.s_addr = sock.INADDR_ANY;
        s.addr.sin_port = sock.htons(8080);
        uint addrlen = s.addr.sizeof;
        sock.sockaddr* addrptr = cast(sock.sockaddr*)&s.addr;
        if(sock.bind(s.s, addrptr,addrlen)<0){
            throw new Exception("bind(...) failed");
        }
        if (sock.listen(s.s, 30) < 0) 
        { 
            throw new Exception("listen"); 
        }
        return s;
    }
    this(){
        //        //s.writeln;
    }
    this(int sock){
        s=sock;
    }
    auto send(void[] buf){

        return sock.send(s,buf.ptr,buf.length,sock.MSG_NOSIGNAL);
    }
    auto recv(void[] buf){
 //       "%d %d %d".writefln(s,buf.length,buf.sizeof);
        return sock.recv(s,buf.ptr,buf.length,sock.MSG_NOSIGNAL);
    }
    void send(Serializable s){
        s.send(this);
    }
    void recv(Serializable s){
        s.recv(this);
    }
    MySocket accept(){
        auto r = new MySocket;
        auto size = cast(uint)addr.sizeof;
        r.s  = sock.accept(s,cast(sock.sockaddr*)&addr, &size);
        return r;
    }
    void connect(sock.Address addr){
        sock.connect(s, addr.name, addr.nameLen);
    }
    void close(){
        .close(s);
    }
}
